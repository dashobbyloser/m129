# Dokumentation mit Markdown und Git

[Markdown](https://de.wikipedia.org/wiki/Markdown) ist die führende Aufzeichnungssprache, wenn es um die Systemdokumentation geht. Der Syntax ist einfach und intuitiv. 

Markdown-Dateien werden automatisch von GitLab gerendert. 

Im Netz sind verschiedene Markdown Editoren. Es wird grundsätzlich zwischen zwei verschiedenen Arten unterschieden: 
 - [WYSIWYG](https://de.wikipedia.org/wiki/WYSIWYG) Editoren
 - Code Editoren mit [*Syntax Highlighting*](https://de.wikipedia.org/wiki/Syntaxhervorhebung)

Markdown ist ein textbasiertes Dateiformat und ist deshalb für den Einsatz mit Git geeignet. 

In den Unterlagen des Moduls 231 finden Sie eine [Einführung zu Git und Markdown](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/02_git).

In dem nachfolgenden Video wird gezeigt, wie Sie einen...
- GitLab Account anlegen, 
- ein GitLab Projekt erstellen,
- Visual Studio Code und Git installieren,
- ein SSH Schlüsselpaar erstellen und den Public-Key ihrem GitLab Account hinzufügen,
- zusätzliche Markdown Plugins in VS Code installieren,
- das Template in ihrem Projekt einfügen,
- den ersten Commit erstellen und pushen,
- E-Mail und Benutzername in der Git Konfiguration für Ihren lokalen Benutzeraccount festlegen.

![Video zu VS Code Git Markdown GitLab](videos/CodeGitMarkdown.webm)