# Repetition Quiz

Prüfen Sie mithilfe des Quiz ihren Lernstand.

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Eigener Lernstand prüfen |
| Zeitbudget  |  15 Minuten |
| Ziele | Eigener Lernstand zu den Themen aus den vorhergehenden Modulen überprüft. |

[Microsoft Forms Quiz](https://forms.office.com/e/CcHqCHA75V)

**Tipp:** Halten Sie in ihrem Lernjournal fest bei welchen Themen sie sich unsicher fühlen und schauen Sie sich anschliessend Ressourcen dazu an. 

# Link zu Ressourcen:
 - [DHCP](https://gitlab.com/ch-tbz-it/Stud/m123/-/tree/main/05_DHCP) - Theorie vom Modul 123
 - [DNS](https://gitlab.com/ch-tbz-it/Stud/m123/-/tree/main/06_DNS) - Theorie vom Modul 123

