# Übung - Switching - BeASwitch

Ein Switch ist ein Kopplungselement in Rechnernetzen, das Netzwerksegmente miteinander verbindet. Mit dem Programm BeASwitch übernehmen Sie die Funktion des Switching Chips und müssen die Switching Entscheidungen treffen und gleichzeitig die MAC-Tabelle befüllen. 

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie verstehen die grundlegende Funktion eines Switches. |

![Screenshot BeASwitch](https://github.com/muqiuq/BeASwitch/raw/main/manual/media/BeASwitch_Explanation.PNG)

## Vorgehen
 - Laden Sie sich auf https://github.com/muqiuq/BeASwitch/releases die neuste Version der Applikation *BeASwitch* herunter.
 - Starten Sie das EXE (Anwendung ist nicht signiert und muss manuell vertraut werden!)<br>**Eventuell müssen Sie noch .NET Core installieren.**
 - Starten Sie im *Pratice Mode* und versuchen Sie durch Trial-and-Error herauszufinden, wie die Anwendung funktioniert.
 - Sobald Sie herausgefunden haben, wie es funktioniert (und damit auch konzeptionell verstanden haben, wie ein Switch funktioniert) welchen Sie in den Exam Mode. Setzen Sie *Minimum number of correct solved challenges* auf 27 und *number of challenges to be solved* auf 30. Sobald sie es geschafft haben, erhalten Sie ein Success Certificate, dass sie anschliessend hochladen können (einfach E-Mail Adresse eingeben und übermitteln). 

**Hinweis: Spielen Sie mit BeASwitch als ob es ein Rätsel wäre. Sie brauchen keine speziellen IT-Kenntnisse, um Herauszufinden wie die Anwendung funktioniert.**

