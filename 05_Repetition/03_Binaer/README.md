# Übung - Binäres Zahlensystem

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Binäre Zahlen spielerisch üben |
| Zeitbudget  |  30 Minuten |
| Ziele | Grundwissen zu binäre Zahlen repetieren. |

Im Modul 129 spielen [binäre Zahlen](https://de.wikipedia.org/wiki/Dualsystem) eine zentrale Rolle. Um IPv4 zu verstehen, gilt das Verständnis über das Dualsystem als Grundlage. 

Die Grundkenntnisse zu den Zahlensystemen werden im Mathematik Unterricht vermittelt. 

Spiele zur Repetition: 
 - Easy - [Cisco Learning Binary Game](https://learningcontent.cisco.com/games/binary/index.html)
 - Fortgeschritten - [Squashbyte](https://www.squashbyte.com/)



