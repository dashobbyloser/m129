# Repetition

Als Grundlage für das Modul *129 LAN-Komponenten in Betrieb nehmen* dienen die Module
 - 117 Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren
 - 123 Serverdienste in Betrieb nehmen

Diverse in diesen Modulen erlernte Handlungskompetenzen sind wichtige Grundlagen für das Modul 129:
 - Kennt die verbreiteten lokalen Netzwerkdienste und kann aufzeigen, welche Anforderungen an ein Netzwerk sich daraus ergeben.
 - Kennt die wichtigsten Regeln für eine korrekte Netzwerkkonfiguration (IP-Adressformat, Subnetzmaske, private Adressen, Standardgateway, DNS) und kann diese anhand von Beispielen erläutern.
 - Kennt die prinzipiellen Aufgaben der Netzwerkkomponenten Switch, Accesspoint und Router und kann aufzeigen, wo und zu welchem Zweck diese in einem Netzwerk eingesetzt werden.
 - Kennt die notwendigen Einstellungen der Netzwerkkonfiguration der Netzwerkkomponenten (z.B. Computer, Router, Accesspoint) und kann aufzeigen, welchen Beitrag diese zur Sicherstellung der Kommunikation im Netzwerk leisten.
 - Kennt die wichtigsten Informationen in der Dokumentation eines einfachen Netzwerks und kann erläutern, wie diese für die Wartung und den Betrieb benötigt werden.
 - Kennt die Möglichkeiten das Netzwerk und all seine Elemente zu testen und die Ergebnisse zu dokumentieren.
 - Kennt die Einstellungen zur Konfiguration eines DHCP Servers und kann erläutern, wie diese Einstellgrössen die Zuweisung einer IP Adresse, einer Subnet-Maske und allenfalls Angaben zu DNS-Servern und Standard-Gateways bei der Anfrage eines Clients beeinflussen.
 - Kennt die Einstellungen zur Konfiguration eines DNS-Servers und kann erläutern, wie diese Einstellgrössen das Verfahren bei der Umsetzung von Namen in IP Adressen (Namensauflösung mittels 'Hosts', DNS) beeinflussen resp. sicherstellen.

# Übungen

 - [Quiz](./01_Quiz/README.md)
 - [BeASwitch](./01_Switching/README.md) - Wer Switching versteht ist auch in der Lage *ein Switch zu sein*
 - [Binäre Zahlen](./03_Binaer/README.md)