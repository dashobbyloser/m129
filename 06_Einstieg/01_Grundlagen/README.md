# Grundlagen

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag mit Quiz |
| Zeitbudget  |  1 Lektion |
| Ziele | Grundlagen für das Modul 129 und wichtigste Begriffe erarbeiten |

Kommunikation pulsiert im Herzen der menschlichen Existenz, ein unerschütterliches Bedürfnis, das uns mit dem Gewebe des Lebens selbst verbindet. Sie ist die Essenz, die es nicht nur dem Menschen, sondern auch zahlreichen anderen Spezies ermöglicht, in einer Symphonie der Zusammenarbeit zu agieren. Durch die endlosen Flüsse der Zeit haben sich Menschen unermüdlich den Herausforderungen gestellt, Distanzen mit Worten zu überbrücken. Vom Rauchsignal, das über majestätische Berge hinweg flüstert, bis hin zu den digitalen Impulsen, die in Lichtgeschwindigkeit durch das unermessliche Netz der Moderne zischen, haben wir unsere Fähigkeit zur Übermittlung von Gedanken und Gefühlen stetig weiterentwickelt. Diese Odyssee der Kommunikation spiegelt unsere unstillbare Sehnsucht wider, miteinander verbunden zu sein, Ideen auszutauschen und kollektiv über das Rauschen der Isolation hinwegzuschreiten.

Die Basis der IP-basierten Kommunikation, die vor allem im zivilen Sektor zahlreiche andere Technologien abgelöst hat und eine der wichtigsten Säulen der globalen Vernetzung darstellt, wurde durch das ARPANet gelegt. Dieses Netzwerk, das ab 1968 im Auftrag der US Air Force entwickelt wurde, ebnete den Weg für die digitale Revolution. ARPANet, der Vorläufer des heutigen Internets, spielte eine entscheidende Rolle bei der Entwicklung des Internetprotokolls (IP), das heute die Grundlage für den Austausch von Daten über das weltweite Netz bildet. Durch die Einführung von IP wurde eine flexible und skalierbare Kommunikation ermöglicht, die es erlaubt, Informationen nahtlos über verschiedene Netzwerke hinweg zu übertragen.

![Interface Message Processor](./medien/Interface_Message_Processor_Front_Panel.jpg)
*Abbildung 1: Der Interface Message Processor (IMP) war der Knotenpunkt für Paketvermittlung, der zur Verbindung von Teilnehmernetzwerken mit dem ARPANET von den späten 1960er Jahren bis 1989 verwendet wurde. *Abbildung 1: Der Interface Message Processor (IMP) war der Knotenpunkt für Paketvermittlung, der zur Verbindung von Teilnehmernetzwerken mit dem ARPANET von den späten 1960er Jahren bis 1989 verwendet wurde. Quasi der erste Router.*
.*

![ARPANET](./medien/Arpanet-1972-Map.png)<br/>
*Abbildung 2: Der vorgänger des Internets 1972 © UCLA Library Digital Collections, CC-BY-4.0*

![he.net 3d MAP](./medien/he3dmap.JPG)<br/>
*Abbildung 3: Übersicht über alle Untersehkabel weltweit und den wichtigsten Internetknoten. Siehe [he.net 3D Map](http://he.net/3d-map/) für eine interaktive Karte.*

In weniger als 52 Jahre (stand 2024) hat sich das Internet von einem kleinen Netzwerk (siehe *Abbildung 2*) in ein weltweites Netzwerk mit milliarden Teilnehmer entwickelt. 

![CAIDA AS Core Poster](./medien/caidaposter.png)<br/>
*Abbildung 4: Übersicht über die Core Router bzw. AS welche den Backbone des Internet bilden. Poster in voller Auflösung siehe [hier](https://www.caida.org/catalog/media/posters/)*

Weitere Ressourcen zur Geschichte des Internets: 

 - [Video - The History of the Internet](https://www.youtube.com/watch?v=9hIQjrMHTv4)
 - [Arpanet - Internet](https://www.sciencemuseum.org.uk/objects-and-stories/arpanet-internet)


# OSI-Modell / TCP-IP Modell

Das OSI- und TCP/IP-Modell dient in erster Linie zur Strukturierung und Standardisierung der Kommunikationsprozesse in Netzwerken, indem es die Datenübertragung in verschiedene Schichten unterteilt. Den auf einer Datenleitung ist jedes verschickte Frame bzw. Paket einfach eine Abfolge von Einsen und Nullen (Siehe *Abbildung 5*). In einer Reihe technischer und organisatorischer Dokumente zum Internet ([RFCs](https://de.wikipedia.org/wiki/Request_for_Comments)) ist die Struktur dieser Frames definiert. In *Abbildung 6* wird einem Teil eines Frames (hexadezimaler Schreibweise) die Bedeutung der einzelnen Bytes zugeordnet. 

![OSI Modell - Datenübertragung](./medien/onthewire.JPG)
<br> *Abbildung 5: Ein Frame dargestellt in hexadezimaler Schreibweise*


![Frame in hexadezimaler Form mit Beschreibungen zu den Feldern](./medien/layer2layer3overview.JPG)
<br> *Abbildung 6: Die Bedeutung einzelner Bytes und Bits variiert und ist teilweise von ihrer Position abhängig. In manchen Fällen bestimmen vorangehende Bytes die Bedeutung der nachfolgenden Bytes.* 

# Übung - Wichtige Technologien
Bevor die wichtigsten Grundlagen von IP, Subnetting und Routing vermittelt werden, hilft es die wichtigsten Begriffe zu repetieren. 

[Forms Quiz - Grundlagen Modul 129](https://forms.office.com/e/sCVJ9y8tsd)

 - Was ist der unterschied zwischen einer dynamischen und einer statischen Adressierung?
 - Welcher Vorteil bringt dynamische Adressierung? Wieso hat man das überhaupt erfunden?
 - Zu welchen Layer im OSI-Modell gehören MAC-Adressen und IP-Adressen?
 - In welchen Zusammenhang stehen IP- und MAC-Adressen?

