# Netzwerkanforderungen aufnehmen und Netzwerkkomponenten bestimmen

Damit ein Netzwerk geplant werden kann, benötigt es nebst der Erfahrung, wichtiges technisches Grundwissen. Dazu gehören Kenntnisse über die technischen Möglichkeiten (Kabel- und Kabellose-Übertragungstechnologien, Gerätetypen, usw.), über deren Einsatzgebiete und Funktionsweisen. Insbesondere muss die Funktionsweise von Switch und Router verstanden sein. 

# Theorie
  - Übertragungstechnologien / Übertragungsmedien: `Herdt_Netzwerke Grundlagen_2022.pdf` (Seite 25 bis 46)
