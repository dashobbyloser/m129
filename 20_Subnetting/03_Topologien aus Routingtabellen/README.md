# Übung - Topologien aus Routing-Tabellen ableiten

![Overview](./media/overview.JPG)

Aus einer Routing-Tabelle kann die Topologie eines Netzwerkes abgeleitet werden. 

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Topologie aus Routing-Tabelle ableiten |
| Zeitbudget  |  1 Lektion |
| Ziele | Verständnis über Routing-Tabellen verstehen |

## Vorgehen
 1. BeARouter starten (BeARouter => Practice Mode)
 2. Leeres [Draw.io](https://draw.io) oder [Microsoft Visio](https://www.microsoft.com/de-ch/microsoft-365/visio/flowchart-software) Dokument erstellen.
 3. Screenshot von der Routing-Tabelle und IP-Adresse-Interface Tabelle erstellen und in Dokument einfügen.
 4. Mithilfe der beiden Tabelle Topologie ableiten. 

Als Beispiel siehe Grafik oben (IP-Adressen-Tabelle nicht abgebildet).