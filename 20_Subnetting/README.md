# Subnetting

Als Subnetting bezeichnet man die Aufteilung eines Netzes in mehrere kleinere Netzwerke. 

Auszug aus der Kompetenzmatrix:
 - C1G: Ich kann in einem Subnetz eine fachgerechtes IP-Adresskonzept für verschiedene Gerätegruppen erstellen.	
 - C1F: Ich kann die IP-Adressierung für mehrere Subnetze in einem LAN fachgerecht planen und umsetzen.	
 - C1E: Ich kann die IP-Adressierung für mehrere Subnetze in einem LAN fachgerecht planen und umsetzen. Ich sehe dabei spezielle Bereiche für Dienste, Geräte (statische IP-Adressierung), sowie Spezialanwendungen vor und zeige zukünftige Erweiterungsmöglichkeiten auf.

Why?
Als Informatiker ist nebst der Informationsverarbeitung, die Kommunikation ein Zentrales Thema. Von der alltägliches Kommunikation über Chat-Apps bis zum Millisekunden schnellen Trading: Fast die gesamte moderne Kommunikation (Zivile Nutzung) erfolgt heutzutage über das Internet. Dem Internet zugrunde liegt das IP Protokoll. Konkret: [IPv4](https://datatracker.ietf.org/doc/html/rfc791) und [IPv6](https://datatracker.ietf.org/doc/html/rfc2460). 

Damit wir als Informatiker Services, welche IPv4 als Grundlage verwenden, konfigurieren können und auch Netzwerke erstellen, betreiben und erweitern können, benötigen wir ein fundiertes Wissen über IPv4 und IPv6.

## Vorgehen - Der Weg zum IPv4-Profi
 - Alle "Grundrechenoperation" beherrschen: Von Hand (ohne elektronischen Geräte) und mit nützlichen Tools
 - Das Konzept und die Vorteile von Subnetting verstehen. 
 - Hands-On: Kleine Netzwerke mit mehreren Subnets planen 

