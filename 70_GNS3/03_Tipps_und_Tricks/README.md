# Tipps und Tricks

# MikroTik - Bridge konfigurieren
Die primäre Funktion eines Routers ist *Routing* zwischen verschiedenen Netzwerken. In der Standardkonfiguration von den meisten Routern ("Home-Router" ausgenommen) sind die einzelnen Ports nicht miteinander auf Layer 2 verknüpft. 

Möchte man, dass ein Router gewisse Ports in einem *virtuellen Switch* zusammennimmt, muss das explizit konfiguriert werden. Die Konfiguration in einem MikroTik erfolgt auf die gleiche Art und weise wie unter Linux. Es wird zuerst eine virtuelle *Bridge* erstellen. Danach werden die gewünschten Ports dieser Bridge zugewiesen.

![Grafik](images/virtualbridge.PNG)
Die Grafik zeigt, wie die physischen Ethernet-Ports in der Software einer virtuelle Bridge zugeordnet werden. 

MikroTik Commands:
```
# add bridge0
/interface bridge add name=bridge0
# attach ports ether3 up to ether4 to bridge0
/interface bridge port add bridge=bridge0 interface=ether2
/interface bridge port add bridge=bridge0 interface=ether3
/interface bridge port add bridge=bridge0 interface=ether4
```

# Unterschied Switch <-> Bridge
Mit [10Base2 (Thinnet)](https://de.wikipedia.org/wiki/10BASE2) und [10Base5 (Thicknet)](https://de.wikipedia.org/wiki/10BASE5) wurden mehrere Netzwerkteilnehmer auf OSI-Layer 1 miteinander verbunden. Sie teilten ein gemeinsames Medium. Zu einem Zeitpunkt konnte immer nur ein Teilnehmer Ethernet-Frames senden oder Empfangen. Wenn zwei Geräte gleichzeitig senden, kommt es zu einer Kollision. Das [CSMA/CD Protokoll](https://de.wikipedia.org/wiki/Carrier_Sense_Multiple_Access/Collision_Detection) regelt das Vorgehen in einem solchen Fall. 

Ab einer gewissen Menge an Datenverkehr (Traffic) und einer gewissen Anzahl Hosts kommt es zu exponentiell vielen Kollisionen. Somit war die Anzahl der Teilnehmer in einer *Kollisionsdomäne* stark begrenzt. Dieses Problem wurde damals mit einer *Bridge* gelöst. Diese koppelte die Netzwerke auf OSI-Layer 2. Eine *Bridge* ist nichts anderes als ein *Zwei-Port-Switch*. 

Heutzutage werden die Begriffe *Bridge* und *Switch* häufig synonym verwendet, da in verkabelten Netzwerken keine *Bridges* im klassischen Sinne eingesetzt werden. 

Das Problem mit den Kollisionen findet man bei Wireless wieder. Dort kommt es ebenfalls zu Kollisionen, wenn zwei Geräte auf der gleichen Frequenz zur selben Zeit senden möchten. 

