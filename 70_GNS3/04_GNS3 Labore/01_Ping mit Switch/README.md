# Labor 1: Ping mit Switch
![Screenshot Labor1](media/Labor1V2.PNG)

## Lernziele
 - **Grundlegende Bedienung GNS3**:
   - Erstellen eines neuen Projektes
   - Hinzufügen von Geräten (VPCS, Switch)
   - Zustandssteuerung: Ein- und Ausschalten von Geräten
   - Beschreibung/Dokumentation von Laboren innerhalb von GNS3 durch den Einsatz von grafischen Elementen und Textblöcken
 - **VPCS**:
   - Konfiguration einer statischen IPv4-Adresse
   - Persistieren der Konfiguration in einer Konfigurationsdatei
   - Ping eines anderen Hosts
 - *Packet trace* mit Wireshark durchführen.

## Anforderungen
- PC1,PC2 und PC3 haben je eine statische IPv4-Adresse
- Alle PCs können sich gegenseitig anpingen
- Verwendetes Subnetz ist vermerkt
- Wireshark Packet Traces wurden durchgeführt

## Vorgehen

### 1. Labor aufbauen
 - Neues GNS3 Projekt eröffnen
 - Benötigte Geräte (VPCS und Switch) einfügen und miteinander verknüpfen

### 2. VPCS IP-Adressen setzen
Die VPCS Adressen können auf mehrere Art und Weisen konfiguriert werden. Entweder die IP-Adresse wird direkt in der Konsole mit dem Befehl `ip address [mask] [gateway]` gesetzt wird oder indem es in der Konfiguration hinterlegt wird (Kontextmenü des entsprechendes VPCS => `Edit config`). <br>Beispiel: Die IP-Adresse `192.168.1.20/24` soll ohne Gateway konfiguriert werden. Dann würde der Befehl lauten: `ip 192.168.1.20 255.255.255.0`

### 3. Ping durchführen
 - Konsole eines beliebigen VPCS öffnen und die beiden anderen Hosts anpingen. 
 - Wenn der ping nicht erfolgreich ist, liegt ein Konfigurationsfehler vor, die Hosts sind nicht gestartet oder die VPCS sind nicht miteinander verbunden. 

### 4. Wireshark Trace durchführen
 - Auf jeder Verbindung zwischen Hosts kann ein *Packet Trace* durchgeführt werden. Im Kontextmenü des Links muss dafür *Start capture* ausgewählt werden. Der Vorgang wird gestartet und Wireshark öffnet sich automatisch. 
[Packet Trace Kontext Menü](media/Wireshark.JPG)
 - Ziel ist es die Pings abzufangen. Nachdem der Trace gestartet wurde, kann von einem VPCS zu einem anderen ein Ping durchgeführt werden. Die *ICMP Echo Request* Pakete tauchen anschliessend im Wireshark auf.
