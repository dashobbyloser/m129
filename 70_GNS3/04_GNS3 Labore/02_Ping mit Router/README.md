# Labor 2: Ping mit Router
![Screenshot Labor1](media/Labor2V2.PNG)

## Lernziele
 - Grundlegende Bedienung eines MikroTik Routers:
   - Erstmaliges Einloggen in einen MirkoTik Router
   - Setzen des Passwortes
   - Zuweisung der Identität
   - Einen Interface eine IPv4-Adresse zuordnen

## Anforderungen
- Zwei unterschiedliche /24 Subnetze
- Router hat jeweils erste IPv4-Adresse
- PC1, PC2 und PC3 haben jeweils eine statische IPv4-Adresse
- Alle PCs können sich gegenseitig anpingen

## Benötigte MikroTik Befehle

Für diese Aufgabe werden nur die aufgeführten MikroTik Befehle benötigt:

 - **Hostname des Routers konfigurieren**: `/system/identity/set name=`<br>Beispiel: `/system/identity/set name=R1`
 - **IP Adresse auf einem Interface konfigurieren:** z.B. 192.168.1.1/24 auf interface *ether1*:
```
/ip address/add interface=ether1 address=192.168.1.1/24
```
 - **Konfigurierte IP-Adressen anzeigen**: `/ip address/print`<br>Beispiel Output:
```
[admin@MikroTik] > /ip address/print
Columns: ADDRESS, NETWORK, INTERFACE
# ADDRESS         NETWORK      INTERFACE
0 192.168.1.1/24  192.168.1.0  ether1
```

## Links
 - https://help.mikrotik.com/docs/display/ROS/Identity
 - https://wiki.mikrotik.com/wiki/Manual:IP/Address