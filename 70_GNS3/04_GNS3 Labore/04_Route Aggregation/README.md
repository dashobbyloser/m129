# Labor 4: GNS3 Labor 4: Aggregierte statische Routen
![Screenshot Labor1](media/Labor4V2.PNG)

## Lernziele
 - Der/die Lernende kann Routen aggregieren

## Anforderungen
 - IPv4 Subnetz A ist wie folgt festgelegt:
   - 192.168.X.0/24
   - X = 24 + Position in Klassenliste
   - Bsp. X = 24 + 1 => 25 => 192.168.15.0/24
 - Subnetz B: 192.168.X.0/24 X = 192 + Pos in KL-Liste
 - Subnetz **D**: 192.168.X.0/24 X = 224 + Pos in KL-Liste
 - Alle VPCS könne alle anderen anpingen
 - Alle VPCS sind vom eigenen Laptop/PC anpingbar.
 - In jedem Netz hat es ein VPCS mit einer Host Adresse aus dem entsprechenden Subnetz.
 - Netz C,E haben eine Maske /30
 - Auf R1 ist für Netz B und D nur eine Route eingetragen
 - Auf R1 ist keine Default Route eingetragen
 - Auf dem eigenen Laptop/PC sind nur zwei zusätzliche Routen eingetragen
 - Alle Subnetze sind klar vermerkt und Abgrenzungen sind grafisch erkennbar

## Links
 - [Routen Aggregieren](../50_IPv4)