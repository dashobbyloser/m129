# Labor 5: Labor mit zwei Router, DHCP Server und Firewall
![Screenshot Labor1](media/Labor5.PNG)

## Lernziele
 - Der/die Lernende kann eine virtuelle Bridge konfigurieren
 - Der/die Lernende kann einen DHCP-Server konfigurieren
 - Der/die Lernende kann Firewall-Regeln definieren

## Anforderungen
Anforderungen:
 - Mind 2 /24 Subnetze (Netz A und Netz B)
 - Netz A: 192.168.X.0/24
   - X = 32 + Position in Klassenliste
 - Netz B: 192.168.X.0/24
   - X = 33 + Position in Klassenliste
 - Jeweils ein DHCP Server für Subnetze *A* und *B*
 - PC1 bis PC4 erhalten IP Adresse via DHCP
 - Jeder PC kann alle anderen PCs und Router anpingen
 - Router haben immer die erste Host-Adresse (Host-min)
 - Kommunikation zwischen R1 und R2 erfolgt via Subnetz *C*
 - Subnetz C muss Maske /30 haben.
 - R1 und R2 sind via Laptop des Lernenden erreichbar.
 - Eine Firewallregel verhindert, dass PC1 und PC2 vom Subnetz 192.168.23.0/24 angepingt werden können. 
 - Auf die Webinterfaces von R1 und R2 kann vom eigenen Laptop/PC zugegriffen werden
 - Alle Subnetze sind klar vermerkt und Abgrenzungen grafisch erkennbar
