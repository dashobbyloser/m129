# Labor 6: NAT
![Screenshot Labor1](media/Labor6.PNG)

## Lernziele
 - Der/die Lernende kann ein Source NAT (Outbound) konfigurieren

## Anforderungen
- PC1 und PC2 verwenden dasselbe Subnetz
- Auf R1 und R2 ist DHCP aktiviert
- PC1 und PC2 können ins Internet pingen
- PC1 und PC2 können DNS auflösen (z.B. ping google.ch)
- Alle Subnetze sind klar vermerkt und Abgrenzungen sind grafisch erkennbar.
- MGMT Netz wird NUR für Management verwenden
(Bei DHCP-Client für eth1 "Add Default Route" deaktivieren)
