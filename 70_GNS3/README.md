![TBZ Cloud Logo](./media/logo.png)

# Praktische Übungen mit GNS3

GNS3 ist ein leistungsstarker und umfangreicher Netzwerk-Emulator, der Emulation, Konfiguration, Testing und Fehlerbehebung von virtuellen und realen Netzwerken ermöglicht. GNS3 kann verwendet werden, um kleine Topologien mit nur wenigen Geräten auf einem Laptop oder grosse Topologien mit vielen Geräten auf mehreren Servern oder sogar in der Cloud auszuführen.

### Installation GNS3 und Zugriff auf TBZ Cloud
 - [01_GNS3 Installation](./01_GNS3%20Installation/)
 - [02_GSN3 Einführung](./02_GNS3%20Einführung/)

### Praktische Laborübungen zum Modul 129
 - [04_GNS3 Labore](./04_GNS3%20Labore/)
